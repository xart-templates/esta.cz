jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');


	$('.mod_menu-main').before('<div class="container"><button class="toggle_menu"/></div>');
	$('.toggle_menu').off().on('click',function(){
		$('body').toggleClass('menu-on');
	});


	$('.mod_articles_category-slideshow').each(function(){
		var i = $('.mod-item',this).length;
		if(i>1){
			$('.mod-items',this).cycle({
				slides: '.mod-item',
				fx: 'fade',
				speed: 900,
				timeout: 5000,
				pauseOnHover: true,
				swipe: true,
				pager: $('.pager ul',this),
				pagerTemplate: '',
				pagerEvent: 'mouseover',
			});
		}
	});

	// UI range slider
	$('.slider_range').each(function(){
		$(this).wrapInner('<div class="slider"/>');
		//$('input',this).hide();
		var data_min = $(this).data('min');
		var data_max = $(this).data('max');
		var data_step = $(this).data('step');
		var data_val = $(this).data('val');
		var data_from = $(this).data('from');
		var data_to = $(this).data('to');
		var input_from = $('[data-input="from"]',this).val();
		var input_to = $('[data-input="to"]',this).val();
		$('.slider',this).before('<span class="slider_from">'+data_from+': <span class="amount">'+input_from+'</span>'+data_val+'</span>');
		$('.slider',this).after('<span class="slider_to">'+data_to+': <span class="amount">'+input_to+'</span>'+data_val+'</span>');
		$('.slider',this).slider({
			range: true,
			min: data_min,
			max: data_max,
			step: data_step,
			values: [input_from,input_to],
			slide: function(event,ui) {
				$('[data-input="from"]',this).val(ui.values[0]);
				$('[data-input="to"]',this).val(ui.values[1]);
				$(this).parents('.slider_range').find('.slider_from .amount').text('').text(ui.values[0]);
				$(this).parents('.slider_range').find('.slider_to .amount').text('').text(ui.values[1]);
			}
		});
	});

	$('.mod_custom-filter .mod-toggle').on('click',function(){
		$(this).parents('.mod_custom-filter').toggleClass('act');
	});

	$('.mod_custom_panes').each(function(){
		$(this).cycle({
			slides: '.pane',
			speed: 1,
			timeout: 0,
			pager: $('.tabs ul',this),
			pagerTemplate: '',
		});
	});


	// checkbox, radio style
	$('[type="checkbox"],[type="radio"]').wrap('<span class="checkbox_wrap"/>').after('<span class="checkbox_btn"/>');

	// select box
	$('select').wrap('<span class="select_wrap"/>');


	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}



	// go top
	var go_top = function(){
		var offset = $(window).scrollTop();
		if(offset>100){
			$('#gotop').addClass('on');
		} else {
			$('#gotop').removeClass('on');
		}
	}
	go_top();
	$(window).scroll(function(){
		go_top();
	});
	$('#gotop a').click(function(){ 
		$('html,body').animate({scrollTop:0},600);
		return false;
	});


	// basictable
	$('table.normal').each(function(index, el) {
		// vetsi tabulka - povol zalamovani
		if ($(this).find('th').length > 1)
		{
			$(this).basictable({
				tableWrapper: true
			});
		}
		// mensi tabulka - nastav breakpoint na maly rozmer
		else
		{
			$(this).basictable({
				tableWrapper: true,
				breakpoint: 120 // css media query
			});
		}
	});



	/* funkce vykonane na pripravenost dokumentu */
	onDocumentReady($);



	/* vycka, nez je dokoncena jakakoliv udalost */
	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();



	/* na opravdovou zmenu velikosti okna - konej */
	window._width = $(window).width();
	$(window).resize(function () {
		waitForFinalEvent(function(){

			// detekce skutecne zmeny
			if (window._width != $(window).width())
			{
				window._width = $(window).width();
				onWindowResize($);
			}

		}, 500, "afterWindowResize");
	});

});


/**
 * funkce vykonane na pripravenost dokumentu
 *
 * @param   {function}  jQ  objekt jQuery
 * 
 * @return
 */
function onDocumentReady(jQ)
{
	uniteHeights(jQ); // vyrovnej vysky elementu
	return;
}


/**
 * funkce vykonane na responzivni chovani
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function onWindowResize(jQ)
{
	uniteHeights(jQ);				// vyrovnej vysky elementu
	return;
}


/**
 * Funkce pro sjednocovani elementu stranky
 * - na zaklade tridy (unite-heights)
 * a nastavene ID skupiny elementu (data-ug)
 *
 * @param   {function}  jQ 	objekt jQuery
 *
 * @return
 */
function uniteHeights(jQ)
{
	// seznam skupin
	var u_gs = [];
	jQ('.unite-heights').each(function(i, el) {
		var c_ug = jQ(this).attr('data-ug');
		if (jQ.inArray( c_ug, u_gs ) == -1)
		{
			u_gs.push(c_ug);
		}
	});

	// projdi vsechny skupiny a nastav jim stejne vysky
	if (u_gs.length > 0)
	{
		jQ.each(u_gs, function(i, g_id) {

			var heighest = 0;
			s = '[data-ug="' + g_id + '"]';	// data unite group

			// zresetuj a nacti nejvyssi
			jQ(s).each(function(index, el) {
				jQ(this).css('height', 'auto');	// reset
				heighest = jQ(this).height() > heighest ? jQ(this).height() : heighest;
			});

			// nastav nejvyssi
			jQ(s).each(function(index, el) {
				jQ(this).height(heighest);
			});
		});
	}
	return;
}